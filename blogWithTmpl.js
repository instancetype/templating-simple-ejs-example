/**
 * Created by instancetype on 6/20/14.
 */
var fs = require('fs')
  , http = require('http')

function getEntries() {
  var entries = []
    , entriesRaw = fs.readFileSync('./entries.txt', 'utf8')

  entriesRaw = entriesRaw.split('---')
  entriesRaw.map(function(entryRaw) {
    var entry = {}
      , lines = entryRaw.split('\n')

    lines.map(function(line) {
      if (line.indexOf('title: ') === 0) {
        entry.title = line.replace('title: ', '')
      }
      else if (line.indexOf('date: ') === 0) {
        entry.date = line.replace('date: ', '')
      }
      else {
        entry.body = entry.body || ''
        entry.body += line
      }
    })
    entries.push(entry)
  })

  return entries
}

var entries = getEntries()
console.log(entries)

var server = http.createServer(function(req, res) {
  var output = blogPage(entries)

  res.writeHead(200, {'Content-Type': 'text/html'})
  res.end(output)
})

server.listen(3000)

// Templating:
var ejs = require('ejs')
  , template = fs.readFileSync('./views/blog_page.ejs', 'utf8')

function blogPage(entries) {
  var values = {entries: entries}
  return ejs.render(template, {locals: values})
}